#ifndef SEPARATOR
#define SEPARATOR

#include <iostream>
#include <string>
#include <map>
#include "Item.h"

using namespace std;

class Separate{
private:
	map<string,string> separations;

public:
	/// Constructor
	Separate() {
		map<string,string>::iterator it = separations.begin();
		separations.insert(it, pair<string,string>("RocketBoots","Boots"));
		separations.insert(it, pair<string,string>("RubberBoots","Boots"));
		separations.insert(it, pair<string,string>("BurgerDrink","Drink"));
	}
	
	/// Destructor
	~Separate(){

	}

	/// Function templates
	Item* fission(string item);
};

#endif
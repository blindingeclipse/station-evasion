#include <iostream>
#include <string>
#include <fstream>

#include "Room.h"

using namespace std;

int MapLocation::getRow(){
	return row;
}

int MapLocation::getCol(){
	return col;
}

bool MapLocation::isPassable(){
	return puzzle.isSolved();
}

void MapLocation::displayDescription(ostream& os){
	os << description << " at co-ordinates " << row << ", " << col << endl << endl;
	displayContainers(os);
}

void MapLocation::displayName(ostream& os){
	os << name;
}

string MapLocation::getName(){
	return name;
}

void MapLocation::setDescription(string desc){
	description = desc;
}

void MapLocation::setPassability(bool pass){
	passable = pass;
}

///Adds a puzzle to the map location
void MapLocation::setPuzzle(string name, string toSolve){
	puzzle.setPuzzle(name, toSolve);
	passable = false;
}

///Adds a puzzle to the map location with a persistent attribute
void MapLocation::setPuzzle(string name, string toSolve, bool p){
	puzzle.setPuzzle(name, toSolve, p);
	passable = false;
}

/**
 * Adds a puzzle to the map location with a persistent attribute, hazard(damage) attribute
 * and a disarmed hazard(damage) value.
 */
void MapLocation::setPuzzle(string name, string toSolve, bool p, int h, int hS){
	puzzle.setPuzzle(name, toSolve, p, h, hS);
	passable = false;
}

Puzzle* MapLocation::getPuzzle(){
	return &puzzle;
}

/**Adds a trap to the map location with a hazard(damage) value and a disarmed
 * hazard(damage) value
 */
void MapLocation::setTrap(string name, string toSolve, int h, int hS){
	trap.setTrap(name, toSolve, h, hS);
}

Trap* MapLocation::getTrap(){
	return &trap;
}

/**
 * Saves a map location to the save file along with its traps, puzzles, and pass-ability
 */
void MapLocation::save(ofstream& outFile){
	outFile << row << " " << col << " " << name << " " << description << " * ";
	outFile << passable << " ";
	trap.save(outFile);
	puzzle.save(outFile);
	subSave(outFile);
}

/**
 * Loads all of the attributes of a map location including
 * traps, puzzles, containers, and pass-ability from a save file
 */
void MapLocation::load(ifstream& inFile){
	description = "";
	string temp = "";
	inFile >> row >> col >> name;
	do{
		inFile >> temp;
		if(temp.compare("*") != 0){
			description += temp;
			description += " ";
		}
	}while((temp).compare("*") != 0);
	inFile >> passable;
	trap.load(inFile);
	puzzle.load(inFile);
	subLoad(inFile);
}

#ifndef EXCEPTIONS_H_
#define EXCEPTIONS_H_

#include <iostream>
#include <stdexcept>

class invalid_go_error : public std::runtime_error{
public:
	invalid_go_error(const char* msg) : std::runtime_error(msg) { }
};

class invalid_look_error : public std::runtime_error{
public:
	invalid_look_error(const char* msg) : std::runtime_error(msg) { }
};

class unsolved_puzzle_error : public std::runtime_error{
public:
	unsolved_puzzle_error(const char* msg) : std::runtime_error(msg) { }
};

class invalid_item_error : public std::runtime_error{
public:
	invalid_item_error(const char* msg) : std::runtime_error(msg) { }
};

#endif
#ifndef CONTAINER
#define CONTAINER

#include <iostream>
#include <string>
#include <vector>

#include "Item.h"

using namespace std;

class Container{
private:
	string description;
	vector<Item*> items;

public:
	/// Constructor
	Container(string desc){
		description.assign(desc);
	}

	Container(){

	}

	/// Destructor
	~Container(){
		// for(unsigned int i=0; i< items.size(); i++){
		// 	delete items[i];
		// }
	}

	/// Function templates
	string getDescription();
	void addItem(string desc);
	void addItem(string desc, int d);
	void addItem(Item* i);
	Item* accessItem(string desc);
	Item* removeItem(string desc);
	void displayItems(ostream& os);
	void save(ofstream& outFile);
	void load(ifstream& inFile);
};

#endif

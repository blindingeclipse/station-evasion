#include <iostream>
#include <string>
#include <fstream>

#include "Puzzle.h"

using namespace std;

/**
 * Solves the puzzle, and sets pass-ability to true
 */
bool Puzzle::solve(string item){
	if(item.compare(requiredItem) == 0){
		if(!persistent)
			solved = true;
		return true;
	}else{
		return false;
	}
	
}

/**
 * Returns whether the puzzle has been solved. Often used to determine pass-ability of a puzzle
 */
bool Puzzle::isSolved(){
	return solved;
}

/**
 * Sets the puzzle with a description and an item required
 * Non-persistent puzzles with no hazard value
 */
void Puzzle::setPuzzle(string name, string toSolve){
	description.assign(name);
	requiredItem.assign(toSolve);
	solved = false;
	persistent = false;
	hazard = 0;
	hazardSave = 0;
}

/**
 * Sets the puzzle with a description, the required item, and a persistent attribute
 * (No hazard)
 */
void Puzzle::setPuzzle(string name, string toSolve, bool p){
	description.assign(name);
	requiredItem.assign(toSolve);
	solved = false;
	persistent = p;
	hazard = 0;
	hazardSave = 0;
}

/**
 * Sets the puzzle with a description, the required item, a persistent attribute, a hazard(damage) value,
 * and the disarmed hazard(damage) value.
 */
void Puzzle::setPuzzle(string name, string toSolve, bool p, int h, int hS){
	description.assign(name);
	requiredItem.assign(toSolve);
	solved = false;
	persistent = p;
	hazard = h;
	hazardSave = hS;
}

///Show the player the puzzle description
void Puzzle::displayPuzzleInfo(ostream& os){
	os << "You found -" << description << "- ";
}
///Show the player the puzzle description
void Puzzle::displayPuzzleName(ostream& os){
	os << " -" << description << "- ";
}

///Saves the state of a puzzle to a save file
void Puzzle::save(ofstream& outFile){
	outFile << solved << " " << persistent << " " << description << " * ";
	outFile << requiredItem << " " << hazard << " " << hazardSave << " ";
}

///Loads the puzzle from a save file
void Puzzle::load(ifstream& inFile){
	inFile >> solved >> persistent;

	string temp = "";
	description.assign("");
	do{
		inFile >> temp;
		if(temp.compare("*") != 0){
			description += temp;
			description += " ";
		}
	}while((temp).compare("*") != 0);

	inFile >> requiredItem >> hazard >> hazardSave;
}

#include <iostream>

#include "NPC.h"

using namespace std;

string Npc::getDescription(){
	return description;
}

string Npc::getName(){
    return name;
}

///Gives the NPC its initial attributes
/*void Npc::setNPC(string n, string desc, string r){
	name.assign(n);
	description.assign(desc);
	requiredItem.assign(r);
	initialized = true;
}*/

bool Npc::exists(){
	return initialized;
}
#include <iostream>
#include <string>

#include "Separate.h"

using namespace std;

Item* Separate::fission(string item){

bool found;
string salvage = "";

	if(separations.find(item) == separations.end() ) {
	    // not found
		found = false;
	} else {
	    // found
		salvage += separations.at(item);
		found = true;
	}

	if(found){
		cout << endl << "FISSION!!!! You salvaged " << salvage << " from ";
		cout << item << endl << endl;
		Item* i = new Item(salvage);
		return i;
	}else{
		return NULL;
	}
}
#ifndef TRAP
#define TRAP

#include <iostream>
#include <string>
#include "Item.h"

using namespace std;

class Trap{
private:
	bool armed = false;
	string description = ".";
	string requiredItem = ".";
	int hazard = 0;
	int hazardSave = 0;

public:
	/// Constructor
	Trap() {
		armed = false;
	}

	/// Destructor
	~Trap(){

	}

	/// Function templates
	int disarm(string item);
	int disarm();
	bool isArmed();
	void setTrap(string name, string toSave, int h, int hS);
	void displayTrapInfo(ostream& os);
	int getHazard();
	int getHazardSave();
	void save(ofstream& outFile);
	void load(ifstream& inFile);
};

#endif

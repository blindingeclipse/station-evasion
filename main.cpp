#include <iostream>

#include "Game.h"

using namespace std;

int main(){
	
	cout << "Starting Game...\n";
	Game* game = new Game();
	game->setup();
	game->play();
	
	return 0;
}
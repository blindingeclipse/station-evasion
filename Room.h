#ifndef ROOM
#define ROOM

#include <iostream>
#include <string>
#include <vector>

#include "NPC.h"
#include "MapLocation.h"
#include "Container.h"
#include "Item.h"
#include "Puzzle.h"

using namespace std;

class Room : public MapLocation {
private:
	vector<Container*> containers;
	Npc* npc;

public:
	/// Constructor
	Room(int r, int c)
	: MapLocation(r,c,"Room")
	{
		setDescription("You see a real spooky spaceship room that might have aliens");
		///Every room has a container "Floor"
		addContainer("Floor");
		setPassability(true);
		npc = new EmptyNPC();
	}

	/// Destructor
	~Room(){
		// delete npc;
		// for(unsigned int i=0; i < containers.size(); i++){
		// 	delete containers[i];
		// }
	}

	/// Function templates
	void addContainer(string desc);
	Container* accessContainer(string desc);
	void displayContainers(ostream& os);
	void setNPC(Npc* anNpc);
	Npc* accessNPC();
	void displayNPC(ostream& os);
	void subSave(ofstream& outFile);
	void subLoad(ifstream& inFile);
};

#endif

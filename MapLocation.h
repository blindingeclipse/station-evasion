#ifndef LOCATION
#define LOCATION

#include <iostream>
#include <string>

#include "NPC.h"
#include "Puzzle.h"
#include "Trap.h"
#include "Container.h"

using namespace std;

class MapLocation{
private:
	/// Has a row and column to identify where it is on the map
	int row, col;
	string name = ".";;
	string description = ".";
	bool passable;
	Trap trap;
	Puzzle puzzle;

public:
	/// Constructor
	MapLocation(int r, int c, string n) {
		row = r;
		col = c;
		name.assign(n);
		passable = false;
	}
	
	/// Destructor
	virtual ~MapLocation() {};

	/// Function templates
	int getRow();
	int getCol();
	bool isPassable();
	void displayDescription(ostream& os);
	void displayName(ostream& os);
	string getName();
	void setDescription(string desc);
	void setPassability(bool pass);
	void setPuzzle(string name, string toSolve);
	void setPuzzle(string name, string toSolve, bool p);
	void setPuzzle(string name, string toSolve, bool p, int h, int hS);
	void setTrap(string name, string toSolve, int h, int hS);
	Puzzle* getPuzzle();
	Trap* getTrap();
	void resolveTrap();
	void save(ofstream& outFile);
	void load(ifstream& inFile);
	virtual void addContainer(string name) =0;
	virtual Container* accessContainer(string name) =0;
	virtual void displayContainers(ostream& os) =0;
	virtual void setNPC(Npc* anNpc) =0;
	virtual Npc* accessNPC() =0;
	virtual void displayNPC(ostream& os) =0;
	virtual void subSave(ofstream& outFile) =0;
	virtual void subLoad(ifstream& inFile) =0;
};

#endif
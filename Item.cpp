#include <iostream>
#include <string>
#include <string.h>
#include <fstream>

#include "Item.h"

using namespace std;

/**
 * Returns the description of an item when the player looks at their inventory
 * If the item is consumable, also displays the number of uses left
 */
string Item::getDescription(){

	if(!consumable){
		return name;
	}else{
		string nameCopy = name;
		string temp = ", Use(s) Left ";
		return (nameCopy += temp += to_string(durability) );
	}
}

string Item::getName(){
	return name;
}

/**
 * Sets an items description (Non-consumable)
 */
void Item::setItem(string description){
	name.assign(description);
	consumable = false;
}

/**
 * Sets an items description (Consumable)
 */
void Item::setItem(string description, int d){
	name.assign(description);
	consumable = true;
	durability = d;
}

/**
 * Using an item reduces durability (number of uses)
 * If durability reaches 0, destroy the item
 */
bool Item::use(){
	if(consumable){
		durability--;
		if(durability < 1){
			return false;
		}
	}
	return true;
}

/**
 * Saves the item to a file with its properties
 */
void Item::save(ofstream& outFile){
	outFile << name << " " << consumable << " " << durability << " ";
}

/**
 * Loads an items properties from a file
 */
void Item::load(ifstream& inFile){
	inFile >> name >> consumable >> durability;
}

#ifndef PUZZLE
#define PUZZLE

#include <iostream>
#include <string>
#include "Item.h"

using namespace std;

class Puzzle{
private:
	bool solved = true;
	bool persistent = false;
	string description = ".";
	string requiredItem = ".";
	int hazard = 0;
	int hazardSave = 0;

public:
	/// Constructor
	Puzzle() {

	}
	
	/// Destructor
	~Puzzle(){

	}

	/// Function templates
	bool solve(string item);
	bool isSolved();
	void setPuzzle(string name, string toSolve);
	void setPuzzle(string name, string toSolve, bool p);
	void setPuzzle(string name, string toSolve, bool p, int h, int hS);
	void displayPuzzleInfo(ostream& os);
	void displayPuzzleName(ostream& os);
	void save(ofstream& outFile);
	void load(ifstream& inFile);
};

#endif
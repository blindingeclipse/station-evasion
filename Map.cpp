#include <iostream>

#include "Map.h"

using namespace std;

/**
 * Returns/Displays specific map location
 */
MapLocation* Map::getSpace(int r, int c){
	// cout << endl << "Room [" << r << "][" << c << "]";
	return space[r][c];
}

/**
 * Returns/Displays map location north of specific map location
 */
MapLocation* Map::northOne(MapLocation* originSpace){
	// cout << endl << "Room [" << r << "][" << c << "]";
	return space[originSpace->getRow()+1][originSpace->getCol()];
}

/**
 * Returns/Displays map location east of specific map location
 */
MapLocation* Map::eastOne(MapLocation* originSpace){
	// cout << endl << "Room [" << r << "][" << c << "]";
	return space[originSpace->getRow()][originSpace->getCol()+1];
}

/**
 * Returns/Displays map location south of specific map location
 */
MapLocation* Map::southOne(MapLocation* originSpace){
	// cout << endl << "Room [" << r << "][" << c << "]";
	return space[originSpace->getRow()-1][originSpace->getCol()];
}

/**
 * Returns/Displays map location west of specific map location
 */
MapLocation* Map::westOne(MapLocation* originSpace){
	// cout << endl << "Room [" << r << "][" << c << "]";
	return space[originSpace->getRow()][originSpace->getCol()-1];
}

/**
 * Creates a room at a map location
 */
void Map::setRoom(int r, int c){
	space[r][c] = new Room(r,c);
}

/**
 * Creates a door at a map location
 */
void Map::setDoor(int r, int c){
	space[r][c] = new Door(r,c);
}

/**
 * Creates a wall at a map location
 */
void Map::setFill(int r, int c){
	space[r][c] = new FilledBlock(r,c);
}

/**
 * Displays the type of map location for each direction that the player
 * can move. E.g. "To the south you see: a wall"
 */
void Map::displaySurroundings(MapLocation* s){
	int baseRow = s->getRow();
	int baseCol = s->getCol();

	cout << "To the north you see: ";
	space[baseRow+1][baseCol]->displayName(cout);
	cout << endl;

	cout << "To the east you see: ";
	space[baseRow][baseCol+1]->displayName(cout);
	cout << endl;

	cout << "To the south you see: ";
	space[baseRow-1][baseCol]->displayName(cout);
	cout << endl;

	cout << "To the west you see: ";
	space[baseRow][baseCol-1]->displayName(cout);
	cout << endl;
}

/**
 * Saves the map to the save file
 */
void Map::save(ofstream& outFile){
	for (int r = 0; r < rows; r++) {
			for (int c = 0; c < cols; c++) {
				getSpace(r,c)->save(outFile);
			}
		}
}

/**
 * Loads the map from the save file
 */
void Map::load(ifstream& inFile){
	for (int r = 0; r < rows; r++) {
			for (int c = 0; c < cols; c++) {
				getSpace(r,c)->load(inFile);
			}
		}
}

#include <iostream>
#include <fstream>

#include "Container.h"

using namespace std;

/**
 * Displays a description of the container
 */
string Container::getDescription(){
	return description;
}

/**
 * Remove an item from the container, occurs when the player "takes" an item
 */
Item* Container::removeItem(string desc){
	for(unsigned int i=0; i<items.size(); i++){
		if(desc.compare(items[i]->getName()) == 0){
			Item* temp = items[i];
			items.erase(items.begin()+i);
			return temp;
		}
	}
	return NULL;
}

/**
 * Adds an item to the container, occurs during the setup of the game
 */
void Container::addItem(string desc){
	items.push_back(new Item(desc));
}

/**
 * Adds an item to the container, occurs when the player "puts" an item
 * (Consumable)
 */
void Container::addItem(string desc, int d){
	items.push_back(new Item(desc, d));
}

/**
 * Adds an item to the container, occurs when the player "puts" an item
 * (Non-Consumable)
 */
void Container::addItem(Item* i){
	items.push_back(i);
}

/**
 * Allows for manipulation of items in a container
 */
Item* Container::accessItem(string desc){
	for(unsigned int i=0; i<items.size(); i++){
		if(desc.compare(items[i]->getName()) == 0){
			return items[i];
		}
	}
	return NULL;
}

/**
 * Shows the player the items in the container, occurs when the player "looks" at a container
 */
void Container::displayItems(ostream& os){
	os << "In this " << description << " you see:\n";

	for(unsigned int i=0; i<items.size(); i++){
		os << " -" << items[i]->getDescription() << endl;
	}

	if(items.size() == 0){
		os << " -Nothing";
	}

	os << endl << endl;
}

/**
 * Saves the contents of the container to the save file
 */
void Container::save(ofstream& outFile){
	outFile << description << " " << items.size() << " ";
	for(unsigned int i=0; i<items.size(); i++){
		items[i]->save(outFile);
	}
}

/**
 * Loads the contents of the container from a save file
 */
void Container::load(ifstream& inFile){

	inFile >> description;

	int s;
	inFile >> s;
	items.clear();
	items.reserve(s);

	for(int i=0; i<s; i++){
		items.push_back(new Item());
		items[i]->load(inFile);
	}
}

#include <iostream>
#include <string>
#include <fstream>

#include "Trap.h"

using namespace std;

/**
 * Attempts to disarm the trap with an item the player used
 * If the item was incorrect, they will take the hazard value of the trap as damage
 * If the item was correct, the player will take the hazardSave value of the trap as damage
 */
int Trap::disarm(string item){
	armed = false;

	if(requiredItem.compare(item) == 0){
		cout << "\nWell that could have been a lot worse. You took " << hazardSave << " damage\n";
		return hazardSave;
	}
	else{
		cout << "\nMaybe that wasn't the right item... You took " << hazard << " damage\n";
		return hazard;
	}
}

/**
 * Attempts to disarm the trap without an item
 * The player will take the hazard value of the trap as damage
 */
int Trap::disarm(){
	armed = false;

	cout << "\nYou took " << hazard << " damage like a man!\n";
	return hazard;
}

/**
 * Returns whether the trap has been disarmed or not. Often used to determine pass-ability.
 */
bool Trap::isArmed(){
	return armed;
}

/**
 * Sets a trap with a name, a required item, a hazard value, and a disarmed hazard value
 */
void Trap::setTrap(string name, string toSave, int h, int hS){
	description.assign(name);
	requiredItem.assign(toSave);
	armed = true;
	hazard = h;
	hazardSave = hS;
}

/**
 * Show the player the trap information
 */
void Trap::displayTrapInfo(ostream& os){
	os << description;
}

/**
 * Return the damage that a hazard will do upon failed disarm
 */
int Trap::getHazard(){
	return hazard;
}

/**
 * Return the damage that a hazard will do upon successful disarm
 */
int Trap::getHazardSave(){
	return hazardSave;
}

/**
 * Saves the state of a trap to a save file
 */
void Trap::save(ofstream& outFile){
	outFile << armed << " " << description << " * " << requiredItem << " ";
	outFile << hazard << " " << hazardSave << " ";
}

/**
 * Loads the state of a trap from a save file
 */
void Trap::load(ifstream& inFile){
	inFile >> armed;

	string temp = "";

	do{
		inFile >> temp;
		if(temp.compare("*") != 0){
			description += temp;
			description += " ";
		}
	}while((temp).compare("*") != 0);

	inFile >> requiredItem >> hazard >> hazardSave;
}

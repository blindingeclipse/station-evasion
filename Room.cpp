#include <iostream>
#include <string>
#include <fstream>

#include "NPC.h"
#include "Room.h"

using namespace std;

///Adds a container to a room (Game Setup)
void Room::addContainer(string desc){
	containers.push_back(new Container(desc));
}

///Allows for manipulation of containers
Container* Room::accessContainer(string desc){
	for(unsigned int i=0; i<containers.size(); i++){
		if(desc.compare(containers[i]->getDescription()) == 0){
			return containers[i];
		}
	}
	return NULL;
}

///Shows the player the containers in the current room
void Room::displayContainers(ostream& os){
	os << "In this room you see:\n";
	for(unsigned int i=0; i<containers.size(); i++){
		os << " -" << containers[i]->getDescription() << endl;
	}
	os << endl;
}

void Room::setNPC(Npc* anNpc){
	npc = anNpc;
}
	
Npc* Room::accessNPC(){
	return npc;
}
	
void Room::displayNPC(ostream& os){
	if(npc->exists())
	os << "In this room you also see a '" << npc->getName() << "' \n\n";
}

///Saves the items in a container to a save file
void Room::subSave(ofstream& outFile){
	outFile << containers.size() << " ";
	for(unsigned int i=0; i<containers.size(); i++){
		containers[i]->save(outFile);
	}
}

///Loads the items in a container from a save file
void Room::subLoad(ifstream& inFile){

	int s;
	inFile >> s;
	containers.clear();
	containers.reserve(s);

	for(int i=0; i<s; i++){
		containers.push_back(new Container());
		containers[i]->load(inFile);
	}
}

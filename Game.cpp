#include <iostream>
#include <vector>
#include <string>
#include <fstream>

#include "Game.h"

using namespace std;

void Game::setup(){

    ///Give player their starting items
	// player.addItem("BrigKeyCard", 1);

    ///Give rooms their starting items, traps, puzzles, and containers
    // @ 1 1
	gameMap.getSpace(1,1)->accessContainer("Floor")->addItem("Flare", 3);
	gameMap.getSpace(1,1)->addContainer("MedicineCabinet");
	gameMap.getSpace(1,1)->accessContainer("MedicineCabinet")->addItem("MedKit", 2);
	gameMap.getSpace(1,1)->accessContainer("MedicineCabinet")->addItem("Batteries", 1);
	// @ 1 2
	gameMap.getSpace(1,2)->setNPC(new MedConsole());
	gameMap.getSpace(1,2)->addContainer("Corpse");
	gameMap.getSpace(1,2)->accessContainer("Corpse")->addItem("MedKeyCard");
	gameMap.getSpace(1,2)->accessContainer("Corpse")->addItem("StunBaton", 1);
	// @ 1 3
	// @ 1 4
	gameMap.setDoor(1,4);
	gameMap.getSpace(1,4)->setPuzzle("MedBay KeyCard Lock","MedKeyCard", true);
	// @ 1 5
	// @ 1 6
	// @ 1 7
	// @ 1 8
	gameMap.setDoor(1,8);
	gameMap.getSpace(1,8)->setPuzzle("MedBay KeyCard Lock","MedKeyCard", true);
	// @ 1 9
	// @ 1 10
	gameMap.setDoor(1,10);
	gameMap.getSpace(1,10)->setPuzzle("Brig KeyCard Lock", "BrigKeyCard", true);
	// @ 1 11
	// @ 1 12
	// @ 1 13
	// @ 1 14
	// @ 1 15
	// @ 1 16
	gameMap.setFill(1,16);
	// @ 1 17
	gameMap.getSpace(1,17)->setNPC(new TerminatorTodd());
	// @ 1 18
	gameMap.setFill(1,18);
	// @ 2 1
	gameMap.setFill(2,1);
	// @ 2 2
	gameMap.setFill(2,2);
	// @ 2 3
	gameMap.setFill(2,3);
	// @ 2 4
	gameMap.setFill(2,4);
	// @ 2 5
	gameMap.setDoor(2,5);
	gameMap.getSpace(2,5)->setPuzzle("MedBay KeyCard Lock","MedKeyCard", true);
	// @ 2 6
	gameMap.setFill(2,6);
	// @ 2 7
	gameMap.setDoor(2,7);
	// @ 2 8
	gameMap.setFill(2,8);
	// @ 2 9
	// @ 2 10
	gameMap.setFill(2,10);
	// @ 2 11
	gameMap.setFill(2,11);
	// @ 2 12
	gameMap.setFill(2,12);
	// @ 2 13
	gameMap.setDoor(2,13);
	gameMap.getSpace(2,13)->setPuzzle("Brig KeyCard Lock","BrigKeyCard", true);
	// @ 2 14
	gameMap.setFill(2,14);
	// @ 2 15
	// @ 2 16
	gameMap.setFill(2,16);
	// @ 2 17
	gameMap.getSpace(2,17)->setNPC(new PrisonConsole());
	gameMap.getSpace(2,17)->addContainer("Corpse");
	gameMap.getSpace(2,17)->accessContainer("Corpse")->addItem("Flare", 2);
	// @ 2 18
	gameMap.getSpace(2,18)->setNPC(new AlcoholicAndy());
	// @ 3 1
	gameMap.getSpace(3,1)->addContainer("Satchel");
	gameMap.getSpace(3,1)->accessContainer("Satchel")->addItem("MedKit", 1);
	gameMap.getSpace(3,1)->accessContainer("Satchel")->addItem("Boots");
	// @ 3 2
	// @ 3 3
	gameMap.getSpace(3,3)->setPuzzle("Burning Hallway", "FireExtinguisher");
	// @ 3 4
	// @ 3 5
	gameMap.getSpace(3,5)->setTrap("Really Hot Steam coming from a broken pipe!", "CANNOTSAVE", 10, 10);
	// @ 3 6
	gameMap.setFill(3,6);
	// @ 3 7
	gameMap.getSpace(3,7)->addContainer("Closet");
	gameMap.getSpace(3,7)->accessContainer("Closet")->addItem("Alcohol", 1);
	gameMap.getSpace(3,7)->accessContainer("Closet")->addItem("Mop");
	gameMap.getSpace(3,7)->accessContainer("Closet")->addItem("SprayBottle");
	gameMap.getSpace(3,7)->accessContainer("Closet")->addItem("Gloves");
	// @ 3 8
	gameMap.setFill(3,8);
	// @ 3 9
	gameMap.getSpace(3,9)->accessContainer("Floor")->addItem("BloodyMess");
	// @ 3 10
	gameMap.setFill(3,10);
	// @ 3 11
	gameMap.getSpace(3,11)->addContainer("NightStand");
	gameMap.getSpace(3,11)->addContainer("Bed");
	gameMap.getSpace(3,11)->accessContainer("NightStand")->addItem("Alcohol");
	gameMap.getSpace(3,11)->accessContainer("NightStand")->addItem("Mop");
	gameMap.getSpace(3,11)->accessContainer("NightStand")->addItem("SprayBottle");
	gameMap.getSpace(3,11)->accessContainer("Bed")->addItem("DirtySheets");
	gameMap.getSpace(3,11)->accessContainer("Bed")->addItem("SandwichCrumbs");
	// @ 3 12
	gameMap.setDoor(3,12);
	// @ 3 13
	// @ 3 14
	gameMap.setFill(3,14);
	// @ 3 15
	// @ 3 16
	gameMap.setFill(3,16);
	// @ 3 17
	gameMap.setDoor(3,17);
	gameMap.getSpace(3,17)->setPuzzle("Brig Key Card Lock", "BrigKeyCard", true);
	// @ 3 18
	gameMap.setFill(3,18);
	// @ 4 1
	gameMap.getSpace(4,1)->setPuzzle("LAZER BEAMS!", "SprayBottle");
	// @ 4 2
	gameMap.setFill(4,2);
	// @ 4 3
	gameMap.setFill(4,3);
	// @ 4 4
	gameMap.setFill(4,4);
	// @ 4 5
	gameMap.getSpace(4,5)->accessContainer("Floor")->addItem("MangledGore");
	// @ 4 6
	gameMap.setFill(4,6);
	// @ 4 7
	gameMap.setFill(4,7);
	// @ 4 8
	gameMap.setFill(4,8);
	// @ 4 9
	// @ 4 10
	gameMap.setFill(4,10);
	// @ 4 11
	gameMap.setFill(4,11);
	// @ 4 12
	gameMap.setFill(4,12);
	// @ 4 13
	// @ 4 14
	gameMap.setFill(4,14);
	// @ 4 15
	// @ 4 16
	gameMap.getSpace(4,16)->setPuzzle("Water With Exposed Wires...", "RubberBoots");
	// @ 4 17
	// @ 4 18
	// @ 5 1
	// @ 5 2
	// @ 5 3
	gameMap.getSpace(5,3)->setPuzzle("Rabid Robot", "StunBaton");
	gameMap.getSpace(5,3)->addContainer("DeadRobot");
	gameMap.getSpace(5,3)->accessContainer("DeadRobot")->addItem("Batteries", 1);
	gameMap.getSpace(5,3)->accessContainer("DeadRobot")->addItem("Batteries", 1);
	gameMap.getSpace(5,3)->accessContainer("DeadRobot")->addItem("Metal");
	// @ 5 4
	// @ 5 5
	// @ 5 6
	gameMap.setFill(5,6);
	// @ 5 7
	gameMap.setFill(5,7);
	// @ 5 8
	gameMap.setFill(5,8);
	// @ 5 9
	// @ 5 10
	gameMap.setFill(5,10);
	// @ 5 11
	gameMap.getSpace(5,11)->setPuzzle("Dark Room", "Flare", true);
	gameMap.getSpace(5,11)->addContainer("Closet");
	gameMap.getSpace(5,11)->accessContainer("Closet")->addItem("Coat");
	gameMap.getSpace(5,11)->accessContainer("Closet")->addItem("Rocket");
	gameMap.getSpace(5,11)->accessContainer("Closet")->addItem("Flare", 2);
	// @ 5 12
	gameMap.setDoor(5,12);
	// @ 5 13
	// @ 5 14
	gameMap.setFill(5,14);
	// @ 5 15
	gameMap.setFill(5,15);
	// @ 5 16
	gameMap.setFill(5,16);
	// @ 5 17
	gameMap.setFill(5,17);
	// @ 5 18
	gameMap.setFill(5,18);
	// @ 6 1
	gameMap.setDoor(6,1);
	// @ 6 2
	gameMap.setFill(6,2);
	// @ 6 3
	gameMap.setFill(6,3);
	// @ 6 4
	gameMap.setFill(6,4);
	// @ 6 5
	gameMap.setDoor(6,5);
	// @ 6 6
	gameMap.setFill(6,6);
	// @ 6 7
	gameMap.setFill(6,7);
	// @ 6 8
	gameMap.setFill(6,8);
	// @ 6 9
	// @ 6 10
	gameMap.setFill(6,10);
	// @ 6 11
	gameMap.setFill(6,11);
	// @ 6 12
	gameMap.setFill(6,12);
	// @ 6 13
	// @ 6 14
	gameMap.setFill(6,14);
	// @ 6 15
	// @ 6 16
	// @ 6 17
	// @ 6 18
	gameMap.getSpace(6,18)->setPuzzle("Command Key Card Lock", "CommandKeyCard");
	// @ 7 1
	gameMap.getSpace(7,1)->setPuzzle("Dark Room", "Flare", true);
	gameMap.getSpace(7,1)->addContainer("NightStand");
	gameMap.getSpace(7,1)->accessContainer("NightStand")->addItem("Rubber");
	gameMap.getSpace(7,1)->accessContainer("NightStand")->addItem("Rubber");
	gameMap.getSpace(7,1)->accessContainer("NightStand")->addItem("MedKit", 2);
	// @ 7 2
	gameMap.setFill(7,2);
	// @ 7 3
	gameMap.getSpace(7,3)->addContainer("Fridge");
	gameMap.getSpace(7,3)->accessContainer("Fridge")->addItem("Burger", 1);
	gameMap.getSpace(7,3)->accessContainer("Fridge")->addItem("Drink", 1);
	// @ 7 4
	gameMap.setFill(7,4);
	// @ 7 5
	// @ 7 6
	gameMap.setFill(7,6);
	// @ 7 7
	gameMap.setFill(7,7);
	// @ 7 8
	gameMap.setFill(7,8);
	// @ 7 9
	// @ 7 10
	gameMap.setFill(7,10);
	// @ 7 11
	// @ 7 12
	gameMap.setDoor(7,12);
	// @ 7 13
	// @ 7 14
	gameMap.setFill(7,14);
	// @ 7 15
	// @ 7 16
	gameMap.getSpace(7,16)->accessContainer("Floor")->addItem("Rocket");
	// @ 7 17
	gameMap.getSpace(7,17)->addContainer("Corpse");
	gameMap.getSpace(7,17)->accessContainer("Corpse")->addItem("CommandKeyCard");
	// @ 7 18
	// @ 8 1
	gameMap.setDoor(8,1);
	// @ 8 2
	gameMap.setFill(8,2);
	// @ 8 3
	gameMap.setDoor(8,3);
	gameMap.getSpace(8,3)->setPuzzle("Unpowered Door", "Batteries");
	// @ 8 4
	gameMap.setFill(8,4);
	// @ 8 5
	gameMap.setDoor(8,5); //Door
	// @ 8 6
	gameMap.setFill(8,6);
	// @ 8 7
	gameMap.setFill(8,7);
	// @ 8 8
	gameMap.setFill(8,8);
	// @ 8 9
	gameMap.setDoor(8,9); //Door
	// @ 8 10
	gameMap.setFill(8,10);
	// @ 8 11
	gameMap.setFill(8,11);
	// @ 8 12
	gameMap.setFill(8,12);
	// @ 8 13
	gameMap.setDoor(8,13);
	// @ 8 14
	gameMap.setFill(8,14);
	// @ 8 15
	// @ 8 16
	// @ 8 17
	// @ 8 18
	// @ 9 1
	// @ 9 2
	// @ 9 3
	// @ 9 4
	// @ 9 5
	// @ 9 6
	gameMap.setDoor(9,6);
	// @ 9 7
	// @ 9 8
	gameMap.setDoor(9,8);
	// @ 9 9
	// @ 9 10
	gameMap.setDoor(9,10);
	// @ 9 11
	// @ 9 12
	// @ 9 13
	// @ 9 14
	// @ 9 15
	// @ 9 16
	// @ 9 17
	gameMap.getSpace(1,2)->accessContainer("Floor")->addItem("Boots");
	// @ 9 18
	// @ 10 1
	gameMap.setDoor(10,1);
	// @ 10 2
	gameMap.setFill(10,2);
	// @ 10 3
	gameMap.setDoor(10,3);
	// @ 10 4
	gameMap.setFill(10,4);
	// @ 10 5
	gameMap.setDoor(10,5);
	// @ 10 6
	gameMap.setFill(10,6);
	// @ 10 7
	gameMap.setDoor(10,7);
	// @ 10 8
	gameMap.setFill(10,8);
	// @ 10 9
	gameMap.setDoor(10,9);
	// @ 10 10
	gameMap.setFill(10,10);
	// @ 10 11
	gameMap.setFill(10,11);
	// @ 10 12
	gameMap.setDoor(10,12);
	// @ 10 13
	gameMap.setFill(10,13);
	// @ 10 14
	gameMap.setFill(10,14);
	// @ 10 15
	gameMap.setFill(10,15);
	// @ 10 16
	gameMap.setDoor(10,16);
	// @ 10 17
	gameMap.setFill(10,17);
	// @ 10 18
	gameMap.setFill(10,18);
	// @ 11 1
	gameMap.getSpace(11,1)->accessContainer("Floor")->addItem("BrigKeyCard");
	// @ 11 2
	gameMap.setFill(11,2);
	// @ 11 3
	gameMap.getSpace(11,3)->addContainer("Dresser");
	gameMap.getSpace(11,3)->accessContainer("Dresser")->addItem("NicePants");
	gameMap.getSpace(11,3)->accessContainer("Dresser")->addItem("FireExtinguisher", 1);
	// @ 11 4
	gameMap.setFill(11,4);
	// @ 11 5
	// @ 11 6
	gameMap.setFill(11,6);
	// @ 11 7
	// @ 11 8
	gameMap.setFill(11,8);
	// @ 11 9
	// @ 11 10
	gameMap.setFill(11,10);
	// @ 11 11
	// @ 11 12
	// @ 11 13
	gameMap.setFill(11,13);
	// @ 11 14
	// @ 11 15
	gameMap.setDoor(11,15);
	// @ 11 16
	// @ 11 17
	gameMap.setDoor(11,17);
	// @ 11 18
	// @ 12 1
	gameMap.setFill(12,1);
	// @ 12 2
	gameMap.setFill(12,2);
	// @ 12 3
	gameMap.setFill(12,3);
	// @ 12 4
	gameMap.setFill(12,4);
	// @ 12 5
	gameMap.setFill(12,5);
	// @ 12 6
	gameMap.setFill(12,6);
	// @ 12 7
	// @ 12 8
	gameMap.setFill(12,8);
	// @ 12 9
	// @ 12 10
	gameMap.setFill(12,10);
	// @ 12 11
	// @ 12 12
	// @ 12 13
	gameMap.setFill(12,13);
	// @ 12 14
	gameMap.setFill(12,14);
	// @ 12 15
	gameMap.setFill(12,15);
	// @ 12 16
	// @ 12 17
	gameMap.setFill(12,17);
	// @ 12 18
	gameMap.setFill(12,18);
	// @ 13 1
	gameMap.getSpace(13,1)->addContainer("Corpse");
	gameMap.getSpace(13,1)->accessContainer("Corpse")->addItem("EscapePodKeyCard");
	gameMap.getSpace(13,1)->accessContainer("Corpse")->addItem("SpaceSuit");
	// @ 13 2
	gameMap.setFill(13,2);
	// @ 13 3
	gameMap.setFill(13,3);
	// @ 13 4
	// @ 13 5
	// @ 13 6
	gameMap.setDoor(13,6);
	// @ 13 7
	// @ 13 8
	gameMap.setFill(13,8);
	// @ 13 9
	// @ 13 10
	gameMap.setFill(13,10);
	// @ 13 11
	// @ 13 12
	// @ 13 13
	gameMap.setFill(13,13);
	// @ 13 14
	// @ 13 15
	gameMap.setDoor(13,15);
	// @ 13 16
	// @ 13 17
	gameMap.setDoor(13,17);
	// @ 13 18
	// @ 14 1
	gameMap.getSpace(14,1)->setPuzzle("A Wide Chasm... The Floor Is Blown Out", "RocketBoots");
	// @ 14 2
	// @ 14 3
	gameMap.setDoor(14,3);
	// @ 14 4
	// @ 14 5
	// @ 14 6
	gameMap.setFill(14,6);
	// @ 14 7
	gameMap.setDoor(14,7);
	gameMap.getSpace(14,7)->setPuzzle("Command Key Card Lock", "CommandKeyCard");
	// @ 14 8
	gameMap.setFill(14,8);
	// @ 14 9
	// @ 14 10
	gameMap.setFill(14,10);
	// @ 14 11
	gameMap.setFill(14,11);
	// @ 14 12
	gameMap.setDoor(14,12);
	// @ 14 13
	gameMap.setFill(14,13);
	// @ 14 14
	gameMap.setFill(14,14);
	// @ 14 15
	gameMap.setFill(14,15);
	// @ 14 16
	gameMap.setDoor(14,16);
	// @ 14 17
	gameMap.setFill(14,17);
	// @ 14 18
	gameMap.setFill(14,18);
	// @ 15 1
	gameMap.setFill(15,1);
	// @ 15 2
	gameMap.setFill(15,2);
	// @ 15 3
	gameMap.setFill(15,3);
	// @ 15 4
	gameMap.setDoor(15,4);
	// @ 15 5
	gameMap.setFill(15,5);
	// @ 15 6
	gameMap.setFill(15,6);
	// @ 15 7
	// @ 15 8
	gameMap.setFill(15,8);
	// @ 15 9
	// @ 15 10
	gameMap.setFill(15,10);
	// @ 15 11
	// @ 15 12
	// @ 15 13
	// @ 15 14
	gameMap.setFill(15,14);
	// @ 15 15
	// @ 15 16
	// @ 15 17
	gameMap.getSpace(15,17)->setTrap("OMFG It's an alien!", "Flare", 99, 10);
	// @ 15 18
	// @ 16 1
	gameMap.getSpace(16,1)->setPuzzle("A Wide Chasm... The Floor Is Blown Out", "RocketBoots");
	// @ 16 2
	gameMap.getSpace(16,2)->setPuzzle("A Wide Chasm... The Floor Is Blown Out", "RocketBoots");
	// @ 16 3
	gameMap.getSpace(16,3)->setPuzzle("A Wide Chasm... The Floor Is Blown Out", "RocketBoots");
	// @ 16 4
	gameMap.getSpace(16,4)->setPuzzle("A Wide Chasm... The Floor Is Blown Out", "RocketBoots");
	gameMap.getSpace(16,4)->addContainer("FootLocker");
	gameMap.getSpace(16,4)->accessContainer("FootLocker")->addItem("Magnets");
	// @ 16 5
	gameMap.getSpace(16,5)->setPuzzle("A Wide Chasm... The Floor Is Blown Out", "RocketBoots");
	// @ 16 6
	// @ 16 7
	// @ 16 8
	gameMap.setFill(16,8);
	// @ 16 9
	// @ 16 10
	gameMap.setFill(16,10);
	// @ 16 11
	// @ 16 12
	gameMap.setFill(16,12);
	// @ 16 13
	// @ 16 14
	gameMap.setFill(16,14);
	// @ 16 15
	// @ 16 16
	gameMap.setFill(16,16);
	// @ 16 17
	gameMap.setFill(16,17);
	// @ 16 18
	gameMap.setDoor(16,18);
	gameMap.getSpace(16,18)->setPuzzle("EscapePod Key Card Lock", "EscapePodKeyCard");
	// @ 17 1
	gameMap.getSpace(17,1)->setPuzzle("A Wide Chasm... The Floor Is Blown Out", "RocketBoots");
	// @ 17 2
	gameMap.getSpace(17,2)->setPuzzle("A Wide Chasm... The Floor Is Blown Out", "RocketBoots");
	// @ 17 3
	gameMap.getSpace(17,3)->setNPC(new CommandConsole());
	gameMap.getSpace(17,3)->setPuzzle("A Wide Chasm... The Floor Is Blown Out", "RocketBoots");
	// @ 17 4
	gameMap.getSpace(17,4)->setPuzzle("A Wide Chasm... The Floor Is Blown Out", "RocketBoots");
	// @ 17 5
	gameMap.setFill(17,5);
	// @ 17 6
	gameMap.setFill(17,6);
	// @ 17 7
	gameMap.setFill(17,7);
	// @ 17 8
	gameMap.setFill(17,8);
	// @ 17 9
	// @ 17 10
	gameMap.setFill(17,10);
	// @ 17 11
	// @ 17 12
	gameMap.setFill(17,12);
	// @ 17 13
	// @ 17 14
	gameMap.setFill(17,14);
	// @ 17 15
	// @ 17 16
	gameMap.setFill(17,16);
	// @ 17 17
	gameMap.setFill(17,17);
	// @ 17 18
	gameMap.getSpace(17,18)->setNPC(new EscapePod());
	// @ 18 1
	gameMap.getSpace(18,1)->setPuzzle("A Wide Chasm... The Floor Is Blown Out", "RocketBoots");
	// @ 18 2
	gameMap.getSpace(18,2)->setPuzzle("A Wide Chasm... The Floor Is Blown Out", "RocketBoots");
	// @ 18 3
	gameMap.getSpace(18,3)->setPuzzle("A Wide Chasm... The Floor Is Blown Out", "RocketBoots");
	// @ 18 4
	gameMap.getSpace(18,4)->setPuzzle("A Wide Chasm... The Floor Is Blown Out", "RocketBoots");
	// @ 18 5
	gameMap.getSpace(18,5)->setPuzzle("A Wide Chasm... The Floor Is Blown Out", "RocketBoots");
	// @ 18 6
	gameMap.setDoor(18,6);
	// @ 18 7
	// @ 18 8
	gameMap.setDoor(18,8);
	// @ 18 9
	// @ 18 10
	gameMap.setDoor(18,10);
	// @ 18 11
	// @ 18 12
	gameMap.setFill(18,12);
	// @ 18 13
	// @ 18 14
	gameMap.setDoor(18,14);
	// @ 18 15
	// @ 18 16
	gameMap.setFill(18,16);
	// @ 18 17
	// @ 18 18
	
}

void Game::play(){

	cout << "\033[2J\033[1;1H";

	cout << "Welcome to Station Evasion!\n\n";
	cout << "You wake up badly injured in the medical wing of\n";
	cout << "a nearly destroyed spaceship, there is an alien on\n";
	cout << "board that has destroyed nearly every thing and\n";
	cout << "every one. You must survive long enough to escape\n";
	cout << "using the ships few remaining escape pods... good luck\n\n";
	cout << "Would you like to load a save file (yes/no)? ";
	string answer;
	cin >> answer;

	if(answer.compare("yes") == 0){
		load();
	}

	cout << "\033[2J\033[1;1H";

	string action = "";
	string subject = "";

    ///Game loop runs until the player quits, dies, or wins.
	while(player.isAlive() && itsNotOverYet()){

		displaySurroundings(cout);
		action.clear();
		subject.clear();
		cin.clear();
		cout << "\n------------------------Type an action-------------------------\n";
		cin >> action;
		if(action.compare("quit") == 0){
			exit(1);
		}
		cin >> subject;
		cout << "\033[2J\033[1;1H";
		resolveAction(action, subject);
		resolveAlien();
	}

	if(itsNotOverYet())
		cout << "You died.\n";
	else
		cout << "Holy Crap You Won!\n";
}

///Returns the room that the player is in
MapLocation* Game::spaceOf(Player player){
	return gameMap.getSpace(player.getRow(), player.getCol());
}

///Returns the room that the alien is in
MapLocation* Game::spaceOf(Alien alien){
	return gameMap.getSpace(alien.getRow(), alien.getCol());
}

/**
 * Takes in two inputs from the player, first the action or command,
 * second the object to be acted upon.
 * E.g. Go North, Use Flare, Combine Rocket
 * Save and Load only require the command
 */
void Game::resolveAction(string action, string subject){

	if(action.compare("go") == 0){
		goCommand(subject);
	}

	if(action.compare("look") == 0){
		lookCommand(subject);
	}

	if(action.compare("use") == 0){
		useCommand(subject);
	}

	if(action.compare("combine") == 0){
		combineCommand(subject);
	}

	if(action.compare("separate") == 0){
		separateCommand(subject);
	}

	if(action.compare("talk") == 0){
		talkCommand(subject);
	}

	if(action.compare("save") == 0){
		saveCommand();
	}

	if(action.compare("load") == 0){
		loadCommand();
	}
	
	if(action.compare("help") == 0){
        helpCommand();
	}

}

/**
 * Checks if the direction of movement is blocked by a wall, puzzle, or trap.
 * If there is a puzzle or trap, they are resolved here.
 * Moves the player in that direction if it is passable.
 * Also checks to make sure that the direction is legal.
 */
void Game::goCommand(string direction){
	try{
	if(direction.compare("north") == 0){
		if((gameMap.northOne(spaceOf(player))->getName()).compare("Wall") == 0){
			throw invalid_go_error("That is a wall. You did not successfully enter the wall.\n\n");
		}else if(gameMap.northOne(spaceOf(player))->isPassable()){
			player.moveNorth();
		}else{
			gameMap.northOne(spaceOf(player))->getPuzzle()->displayPuzzleInfo(cout);
			if(resolvePuzzle(gameMap.northOne(spaceOf(player))->getPuzzle()))
				player.moveNorth();
		}
	}else if(direction.compare("east") == 0){
		if((gameMap.eastOne(spaceOf(player))->getName()).compare("Wall") == 0){
			throw invalid_go_error("That is a wall. You did not successfully enter the wall.\n\n");
		}else if(gameMap.eastOne(spaceOf(player))->isPassable()){
			player.moveEast();
		}else{
			gameMap.eastOne(spaceOf(player))->getPuzzle()->displayPuzzleInfo(cout);
			if(resolvePuzzle(gameMap.eastOne(spaceOf(player))->getPuzzle()))
				player.moveEast();
		}
	}else if(direction.compare("south") == 0){
		if((gameMap.southOne(spaceOf(player))->getName()).compare("Wall") == 0){
			throw invalid_go_error("That is a wall. You did not successfully enter the wall.\n\n");
		}else if(gameMap.southOne(spaceOf(player))->isPassable()){
			player.moveSouth();
		}else{
			gameMap.southOne(spaceOf(player))->getPuzzle()->displayPuzzleInfo(cout);
			if(resolvePuzzle(gameMap.southOne(spaceOf(player))->getPuzzle()))
				player.moveSouth();
		}
	}else if(direction.compare("west") == 0){
		if((gameMap.westOne(spaceOf(player))->getName()).compare("Wall") == 0){
			throw invalid_go_error("That is a wall. You did not successfully enter the wall.\n\n");
		}else if(gameMap.westOne(spaceOf(player))->isPassable()){
			player.moveWest();
		}else{
			gameMap.westOne(spaceOf(player))->getPuzzle()->displayPuzzleInfo(cout);
			if(resolvePuzzle(gameMap.westOne(spaceOf(player))->getPuzzle()))
				player.moveWest();
		}
	}else{
		throw invalid_go_error("\n\nValid 'go' directions are 'north', 'south', 'east', or 'west'\n\n");
	}
	}catch(invalid_go_error& err){
		cout << err.what();
	}

	resolveTrap(spaceOf(player)->getTrap());
}

/**
 * Allows the player to look or inspect their inventory or a container in the current room.
 * Displays the contents of whatever the player looks at, and returns an error if they look
 * at an invalid object
 */
void Game::lookCommand(string object){

	try{
		if(object.compare("inventory") == 0){
			player.displayItems(cout);
		}else {

			Container* container = spaceOf(player)->accessContainer(object);
			if(container != NULL){
				itemExchangeMenu(container);
			}else{
				throw invalid_look_error("You looked as hard as you could but could find no such container in this room\n\n");
			}
		}
	}catch(invalid_look_error& err){
		cout << err.what();
	}

}

/**
 * Allows the player to use their items. All items except MedKit
 * are meant to be used on objects in the game such as puzzles/traps.
 * If they are consumable, using them reduces their count by 1.
 */
void Game::useCommand(string object){

	cout << "Attempting to use " << object << "...\n";
	bool inInv = player.useItem(object);


	if(inInv){
		cout << endl << object << " Used!\n\n";
		if(object.compare("MedKit") == 0){
			player.heal(50);
		}
	}else{
		cout << endl << object << " Not Used!\n\n";
	}
}

/**
 * Allows the player to combine certain items together
 * E.g. combine Rocket and Boots
 * The items must be in their inventory, and they must be able to be combined
 * On successful fusion, the original items are removed, and the new item is added
 * to their inventory
 */
void Game::combineCommand(string object){

	bool inInv = player.hasItem(object);

	if(!inInv){
		return;
	}

	cout << "What do you want to combine " << object << " with? ";
	string otherObject;
	cin >> otherObject;

	Item* i = combinator.fusion(object, otherObject);
	if(i == NULL){
		cout << "\nYou try your hardest but the " << object;
		cout << " and " << otherObject << " will not FUSION!!!\n\n";

	}else{
		player.removeItem(object);
		player.removeItem(otherObject);
		player.addItem(i);
	}
}

/**
 * Allows the player to separate objects that they have combined.
 * E.g. separate RocketBoots
 * The item must be in their inventory, and must be separable
 * Upon successful fission, the original item is removed and the new items are added
 * to their inventory
 */
void Game::separateCommand(string object){

	bool inInv = player.hasItem(object);

	if(!inInv){
		return;
	}

	Item* i = separator.fission(object);
	if(i == NULL){
		cout << "\nYou try your hardest but the " << object;
		cout << " will not FISSION!!!\n\n";

	}else{
		player.removeItem(object);
		player.addItem(i);
	}
}

void Game::talkCommand(string object){

	if(object.compare(spaceOf(player)->accessNPC()->getName()) == 0){
		spaceOf(player)->accessNPC()->interact(&player);
	}else{
		cout << "You try talking to " << object << " and feel slightly more insane\n\n";
	}
}

/**
 * Saves the games current state to a file
 */
void Game::saveCommand(){

	cout << "would you like to save the game (yes/no)? ";
	string response;
	cin >> response;

	if(response.compare("yes") == 0){
		save();
	}
}

/**
 * Loads a saved file for the player to continue from
 */
void Game::loadCommand(){

	cout << "Would you like to load the saved file (yes/no)? ";
	string response;
	cin >> response;

	if(response.compare("yes") == 0){
		load();
	}
}

/**
 * Displays some basic help for the player in case they are stuck
 */
void Game::helpCommand(){
    cout << "The object of this game is to find the escape pod and escape from the space station without dying. \n"
        << "You must avoid the Alien, disarm traps, and solve puzzles in order to progress through to the end. \n" << endl;
    cout << "Available commands are: \n"
        << "- go (north, south, east, west) \n"
        << "- look (inventory, Floor, Box) \n"
        << "- use (Item) - Must be in your inventory. \n"
        << "- combine (Item) - You will be asked for the second item. \n"
        << "- separate (Item) - Must be a combined item. \n"
        << "- save Game \n"
        << "- load Game \n"
        << "- help \n" << endl;
}

/**
 * Saves the players information to a file
 * Saves the maps information to a file
 * Saves the aliens information to a file
 * Saving overwrites the previous save file
 */
void Game::save(){
	ofstream outFile;
	outFile.open("save.abh");
	player.save(outFile);
	alien.save(outFile);
	gameMap.save(outFile);
	outFile.close();
}

/**
 * Loads the players information from a file
 * Loads the maps information from a file
 * Loads the aliens information from a file
 * There is only one save file available to be loaded from
 */
void Game::load(){
	ifstream inFile;
	inFile.open("save.abh");
	player.load(inFile);
	alien.load(inFile);
	gameMap.load(inFile);
	inFile.close();
}

/**
 * Displays the stats of the player such as HP,
 * as well as the description of the room that the player is in,
 * and the information about adjacent rooms.
 */
void Game::displaySurroundings(ostream& os){
	player.displayStats(cout);
	spaceOf(player)->displayDescription(cout);
	spaceOf(player)->displayNPC(cout);
	gameMap.displaySurroundings(spaceOf(player));
}

/**
 * Allows the player to interact with puzzles by using items
 * on them. If the item is correct, the puzzle will be solved,
 * and the room will now be set to passable
 */
bool Game::resolvePuzzle(Puzzle* puzzle){
	string item;

	cout << "What would you like to use on the";
	puzzle->displayPuzzleName(cout);
	cout << "? ";
	cin >> item;

	if(item.compare("quit") == 0){
		exit(1);
	}

	cout << "Attempting to use " << item << "...\n";
	bool inInv = player.useItem(item);

	if(inInv){
		if(puzzle->solve(item)){
			cout << "It Worked!\n\n";
			return true;
		}else{
			cout << "It Didn't Work!\n\n";
			return false;
		}
	}else{
		return false;
	}
}

/**
 * When a player looks at a container such as a floor, they are able to transfer items
 * between their inventory and the container using this menu. Available commands are
 * take, put, and back.
 */
void Game::itemExchangeMenu(Container* container){
	string action, item;

	while(true){

		cout << "\033[2J\033[1;1H";
		cout << "You can 'take' items from here or 'put' items from your inventory here\ntype 'back' to go back\n\n";
		if(player.inventorySize() >= 10){
			cout << "FYI! Your currently at your inventory limit, you can't carry any more than this\n\n";
		}
		container->displayItems(cout);
		player.displayItems(cout);

		cin >> action;

		if(action.compare("back") == 0){
			cout << endl;
			return;
		}

		cin >> item;

		if(action.compare("take") == 0){
			if(player.inventorySize() < 10)
			takeItem(container, item);
		}else if(action.compare("put") == 0){
			putItem(container, item);
		}
	}
}

/**
 * If the player chooses to take an item from a container, then the item
 * is removed from the container, and added to their inventory.
 */
void Game::takeItem(Container* container, string item){
	Item* i = container->removeItem(item);

	if(i != NULL){
		player.addItem(i);
		cout << "You got a " << item << endl;
	}else{
		cout << item << " not found\n";
	}
}

/**
 * If the player chooses to put an item in a container, then the item
 * is removed from their inventory, and added to the container.
 */
void Game::putItem(Container* container, string item){
	Item* i = player.removeItem(item);

	if(i != NULL){
		container->addItem(i);
		cout << item << " was stored" << endl;
	}else{
		cout << "You don't have " << item << endl;
	}
}

/**
 * If the player runs into a trap, then they are forced to either
 * use an item on the trap, or take damage from the trap.
 * The item required and the damage output upon a failed disarm
 * is set in the trap type.
 */
void Game::resolveTrap(Trap* trap){
	if(!trap->isArmed()){
		return;
	}

	cout << "IT'S A TRAP!" << endl;
	trap->displayTrapInfo(cout);

	cout << " Would you like to use an item on ";
	trap->displayTrapInfo(cout);
	cout << " (yes/no) ? ";

	string response;
	cin >> response;

	if(response.compare("yes") == 0){
		cout << endl << "What item do you want to use? ";
		string item;
		cin >> item;

		cout << "Attempting to use " << item << "...\n";
		bool inInv = player.useItem(item);

		if(inInv){
			player.damage(trap->disarm(item));
		}else{
			player.damage(trap->disarm());
		}
	}else{
		player.damage(trap->disarm());
	}

	cout << endl;
}

bool Game::itsNotOverYet(){
	if(player.hazWon())
		return false;
	else
		return true;
}

void Game::resolveAlien(){
	while(true){
		int dir = alien.move();

		switch(dir){
			case 1:
			if((gameMap.northOne(spaceOf(alien))->getName()).compare("Wall") == 0)
				continue;
			else
				alien.row++;
		break;
			case 2:
			if((gameMap.eastOne(spaceOf(alien))->getName()).compare("Wall") == 0)
				continue;
			else
				alien.col++;
		break;
			case 3:
			if((gameMap.southOne(spaceOf(alien))->getName()).compare("Wall") == 0)
				continue;
			else
				alien.row--;
		break;
			case 4:
			if((gameMap.westOne(spaceOf(alien))->getName()).compare("Wall") == 0)
				continue;
			else
				alien.col--;
		break;
			default:
		break;

		}
		break;
	}

	if(alien.getRow() == player.getRow())
		if(alien.getCol() == player.getCol()){
			cout << "OMFG A Wild Alien Appeared lvl 9001+!\n\n";
			cout << "You have time to try using 1 item what do you use? ";
			string item;
			cin >> item;

			bool inInv = player.useItem(item);

			if(inInv){
				if(item.compare("Flare") == 0){
					cout << "Close one, you might have just peed a little\n";
					alien.move();
				}
			}else{
				player.damage(10000);
			}
		}

}
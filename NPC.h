#ifndef NPC
#define NPC

#include <iostream>
#include <string>
#include <vector>

#include "Item.h"
#include "Player.h"
using namespace std;

class Npc{
private:

public:
	string name = "";
	string description = "";
	string requiredItem = "";
	bool initialized = false;


	/// Constructor
	Npc(){

	}

	/// Destructor
	~Npc(){

	}

	/// Function templates
	string getName();
	string getDescription();
	// void setNPC(string name, string desc, string requiredItem);
	bool exists();
	virtual void interact(Player* p) =0;
};

class EmptyNPC : public Npc{
private:

public:
	/// Constructor
	EmptyNPC(){
		initialized = false;
	}

	/// Destructor
	~EmptyNPC(){

	}

	/// Function templates
	void interact(Player* p){}
};

class MedConsole : public Npc{
private:

public:
	/// Constructor
	MedConsole(){
		initialized = true;
		name = "Computer";
		description = "A run down computer that seems to be barely working glows dimly";
	}

	/// Destructor
	~MedConsole(){

	}

	void interact(Player* p){
		cout << "You attempt to get the computer working and after moments\n";
		cout << "of fans running and loud beeping the screen reads:\n";
		cout << "ERROR_AUTO_AQUISITION_LOCK_WITH_RAISED_IRQL\n";
		cout << "The terminal then seems to freeze on a map screen\n";
		cout << "|w|w|w|w|w|w|w|w|w|w|w|w|w|w|w|w|w|w|w|w|\n";
		cout << "|w| | | | | |d| |d| |d| |w| |d| |w| | |w|\n";
		cout << "|w| | |C| |w|w|w|w| |w| |w| |w| |w|w|E|w|\n";
		cout << "|w| | | | | | | |w| |w| |w| |w| |w|w|d|w|\n";
		cout << "|w|w|w|w|d|w|w| |w| |w| | | |w| | | | |w|\n";
		cout << "|w| | |d| | |w|d|w| |w|w|d|w|w|w|d|w|w|w|\n";
		cout << "|w| |w|w| | |d| |w| |w| | |w| |d| |d| |w|\n";
		cout << "|w|w|w|w|w|w|w| |w| |w| | |w|w|w| |w|w|w|\n";
		cout << "|w| |w| |w| |w| |w| |w| | |w| |d| |d| |w|\n";
		cout << "|w|d|w|d|w|d|w|d|w|d|w|w|d|w|w|w|d|w|w|w|\n";
		cout << "|w| | | | | |d| |d| |d| | | | | | | | |w|\n";
		cout << "|w|d|w|d|w|d|w|w|w|d|w|w|w|d|w| | | | |w|\n";
		cout << "|w| |w| |w| |w|w|w| |w| |d| |w| | | | |w|\n";
		cout << "|w|d|w|w|w|d|w|w|w| |w|w|w| |w| | | | |w|\n";
		cout << "|w| | | | | |w|w|w| |w| |d| |w|w|w|w|w|w|\n";
		cout << "|w| |w|w|w| |w|w|w| |w|w|w| |w| | | | |w|\n";
		cout << "|w| | | | | |w| |w| |w| |d| |w| |w|d|w|w|\n";
		cout << "|w|w|w|w|w|d|w|d|w| |w|w|w|d|w| |w|P| |w|\n";
		cout << "|w| |*| |d| | | |d| |d| | | | | |w| |w|w|\n";
		cout << "|w|w|w|w|w|w|w|w|w|w|w|w|w|w|w|w|w|w|w|w|\n";

		cout << "|w| = wall\n";
		cout << "|d| = door\n";
		cout << "| | = room\n";
		cout << "|*| = Current Location\n";
		cout << "|C| = Command Console \n";
		cout << "|P| = Prison Console\n";
		cout << "|E| = Escape Pods\n";
		
	}
};

class PrisonConsole : public Npc{
private:

public:
	/// Constructor
	PrisonConsole(){
		initialized = true;
		name = "Computer";
		description = "A run down computer that seems to be barely working glows dimly";
	}

	/// Destructor
	~PrisonConsole(){

	}

	void interact(Player* p){
		cout << "You attempt to get the computer working and after moments\n";
		cout << "of fans running and loud beeping the screen reads:\n";
		cout << "ERROR_AUTO_AQUISITION_LOCK_WITH_RAISED_IRQL\n";
		cout << "The terminal then seems to freeze on a map screen\n";
		cout << "|w|w|w|w|w|w|w|w|w|w|w|w|w|w|w|w|w|w|w|w|\n";
		cout << "|w| | | | | |d| |d| |d| |w| |d| |w| | |w|\n";
		cout << "|w| | |C| |w|w|w|w| |w| |w| |w| |w|w|E|w|\n";
		cout << "|w| | | | | | | |w| |w| |w| |w| |w|w|d|w|\n";
		cout << "|w|w|w|w|d|w|w| |w| |w| | | |w| | | | |w|\n";
		cout << "|w| | |d| | |w|d|w| |w|w|d|w|w|w|d|w|w|w|\n";
		cout << "|w| |w|w| | |d| |w| |w| | |w| |d| |d| |w|\n";
		cout << "|w|w|w|w|w|w|w| |w| |w| | |w|w|w| |w|w|w|\n";
		cout << "|w| |w| |w| |w| |w| |w| | |w| |d| |d| |w|\n";
		cout << "|w|d|w|d|w|d|w|d|w|d|w|w|d|w|w|w|d|w|w|w|\n";
		cout << "|w| | | | | |d| |d| |d| | | | | | | | |w|\n";
		cout << "|w|d|w|d|w|d|w|w|w|d|w|w|w|d|w| | | | |w|\n";
		cout << "|w| |w| |w| |w|w|w| |w| |d| |w| | | | |w|\n";
		cout << "|w|d|w|w|w|d|w|w|w| |w|w|w| |w| | | | |w|\n";
		cout << "|w| | | | | |w|w|w| |w| |d| |w|w|w|w|w|w|\n";
		cout << "|w| |w|w|w| |w|w|w| |w|w|w| |w| | | | |w|\n";
		cout << "|w| | | | | |w| |w| |w| |d| |w| |w|d|w|w|\n";
		cout << "|w|w|w|w|w|d|w|d|w| |w|w|w|d|w| |w|*| |w|\n";
		cout << "|w| |M| |d| | | |d| |d| | | | | |w| |w|w|\n";
		cout << "|w|w|w|w|w|w|w|w|w|w|w|w|w|w|w|w|w|w|w|w|\n";

		cout << "|w| = wall\n";
		cout << "|d| = door\n";
		cout << "| | = room\n";
		cout << "|*| = Current Location\n";
		cout << "|C| = Command Console \n";
		cout << "|M| = Medic Console\n";
		cout << "|E| = Escape Pods\n";
		
	}
};

class CommandConsole : public Npc{
private:

public:
	/// Constructor
	CommandConsole(){
		initialized = true;
		name = "Computer";
		description = "A run down computer that seems to be barely working glows dimly";
	}

	/// Destructor
	~CommandConsole(){

	}

	void interact(Player* p){
		cout << "You attempt to get the computer working and after moments\n";
		cout << "of fans running and loud beeping the screen reads:\n";
		cout << "ERROR_AUTO_AQUISITION_LOCK_WITH_RAISED_IRQL\n";
		cout << "The terminal then seems to freeze on a map screen\n";
		cout << "|w|w|w|w|w|w|w|w|w|w|w|w|w|w|w|w|w|w|w|w|\n";
		cout << "|w| | | | | |d| |d| |d| |w| |d| |w| | |w|\n";
		cout << "|w| | |*| |w|w|w|w| |w| |w| |w| |w|w|E|w|\n";
		cout << "|w| | | | | | | |w| |w| |w| |w| |w|w|d|w|\n";
		cout << "|w|w|w|w|d|w|w| |w| |w| | | |w| | | | |w|\n";
		cout << "|w| | |d| | |w|d|w| |w|w|d|w|w|w|d|w|w|w|\n";
		cout << "|w| |w|w| | |d| |w| |w| | |w| |d| |d| |w|\n";
		cout << "|w|w|w|w|w|w|w| |w| |w| | |w|w|w| |w|w|w|\n";
		cout << "|w| |w| |w| |w| |w| |w| | |w| |d| |d| |w|\n";
		cout << "|w|d|w|d|w|d|w|d|w|d|w|w|d|w|w|w|d|w|w|w|\n";
		cout << "|w| | | | | |d| |d| |d| | | | | | | | |w|\n";
		cout << "|w|d|w|d|w|d|w|w|w|d|w|w|w|d|w| | | | |w|\n";
		cout << "|w| |w| |w| |w|w|w| |w| |d| |w| | | | |w|\n";
		cout << "|w|d|w|w|w|d|w|w|w| |w|w|w| |w| | | | |w|\n";
		cout << "|w| | | | | |w|w|w| |w| |d| |w|w|w|w|w|w|\n";
		cout << "|w| |w|w|w| |w|w|w| |w|w|w| |w| | | | |w|\n";
		cout << "|w| | | | | |w| |w| |w| |d| |w| |w|d|w|w|\n";
		cout << "|w|w|w|w|w|d|w|d|w| |w|w|w|d|w| |w|P| |w|\n";
		cout << "|w| |M| |d| | | |d| |d| | | | | |w| |w|w|\n";
		cout << "|w|w|w|w|w|w|w|w|w|w|w|w|w|w|w|w|w|w|w|w|\n";

		cout << "|w| = wall\n";
		cout << "|d| = door\n";
		cout << "| | = room\n";
		cout << "|*| = Current Location\n";
		cout << "|P| = Prison Console \n";
		cout << "|M| = Medic Console\n";
		cout << "|E| = Escape Pods\n";
		
	}
};

class TerminatorTodd : public Npc{
private:

public:
	/// Constructor
	TerminatorTodd(){
		initialized = true;
		name = "Todd";
		description = "A cyborg prisoner seems to be lying helplessly on the ground";
		requiredItem = "BurgerDrink";
	}

	/// Destructor
	~TerminatorTodd(){

	}

	void interact(Player* p){
		string item;

		cout << "\"Hey buddy, got any food and drink?\"\n";
		cout << "What do you want to give Todd?\n";
		cin >> item;

		if(item.compare("quit") == 0){
			exit(1);
		}

		cout << "Attempting to give " << item << "...\n";
		bool inInv = p->useItem(item);

		if(inInv){
			if(requiredItem.compare(item) == 0){
				cout << "Ahhh Thanks! By the way the launch code for the EscapePod is: C1E4Q5\n";
			}
			else{
				cout << "\"I don't want that!\"\n";
				cout << "Todd shoots you with his laser gun.";
				p->damage(30);
			}
		}
	}
};

class AlcoholicAndy : public Npc{
private:

public:
	/// Constructor
	AlcoholicAndy(){
		initialized = true;
		name = "Andy";
		description = "A run-down old man in tattered clothes is sitting in the corner with a bunch of empty bottles scattered around him\n";
		requiredItem = "Alcohol";
	}

	/// Destructor
	~AlcoholicAndy(){

	}

	void interact(Player* p){
		string item;

		cout << "\"Hey buddy, got any drink?\"\n";
		cout << "What do you want to give Andy?\n";
		cin >> item;

		if(item.compare("quit") == 0){
			exit(1);
		}

		cout << "Attempting to give " << item << "...\n";
		bool inInv = p->useItem(item);

		if(inInv){
			if(requiredItem.compare(item) == 0){
				cout << "Ahhh Thanks! By the way the launch code for the EscapePod is: Q5E4C1\n";
			}
			else{
				cout << "\"I don't want that!\"\n";
				cout << "Andy throws an empty bottle and hits you in the head.\n";
				p->damage(10);
			}
		}
	}
};
class EscapePod : public Npc{
private:

public:
	/// Constructor
	EscapePod(){
		initialized = true;
		name = "EscapePod";
		description = "You found the escape pod!";
	}

	/// Destructor
	~EscapePod(){

	}

	void interact(Player* p){
			string code;
			cout << "You approach the console, and suddenly it boots and lights start streaming across the screen. It's asking you to input the launch code. Do you remember?\n";
			cin >> code;
			if(code.compare("C1E4Q5")==0){
				p->didWin();
			}
			else{
				cout << "You entered the wrong code. Lightning shoots out from the console's USB port directly at you \n";
				p->damage(99);
			}
	}
};
#endif

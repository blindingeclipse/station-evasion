#ifndef MAP
#define MAP

#include "MapLocation.h"
#include "Room.h"
#include "Door.h"
#include "FilledBlock.h"

using namespace std;

class Map{
private:
	int rows, cols;
	MapLocation* space[20][20];

public:
	/// Constructor
	Map(){
		rows = 20;
		cols = 20;

        ///Surrounds the map with walls, fills in the map with rooms
		for (int r = 0; r < rows; r++) {
			for (int c = 0; c < cols; c++) {
				if(r == 0 || c == 0 || r == (rows-1) || c == (cols-1)){
					space[r][c] = new FilledBlock(r,c);
				}else{
					space[r][c] = new Room(r,c);
				}
			}
		}
	}

	/// Destructor
	~Map(){
		// for(int i=0; i < rows; i++){
		// 	for(int j=0; j < cols; j++){
		// 		delete space[i][j];
		// 	}
		// }
	}

	/// Function Templates
	MapLocation* northOne(MapLocation* originSpace);
	MapLocation* eastOne(MapLocation* originSpace);
	MapLocation* southOne(MapLocation* originSpace);
	MapLocation* westOne(MapLocation* originSpace);
	MapLocation* getSpace(int r, int c);
	void displaySurroundings(MapLocation* space);
	void setRoom(int r, int c);
	void setDoor(int r, int c);
	void setFill(int r, int c);
	void save(ofstream& outFile);
	void load(ifstream& inFile);
};

#endif

#ifndef COMBINER
#define COMBINER

#include <iostream>
#include <string>
#include <map>
#include "Item.h"

using namespace std;

class Combine{
private:
	map<string,string> combinations;

public:
	/// Constructor
	Combine() {
		map<string,string>::iterator it = combinations.begin();
		combinations.insert(it, pair<string,string>("Rocket","Boots"));
		combinations.insert(it, pair<string,string>("Rubber","Boots"));
		combinations.insert(it, pair<string,string>("Burger","Drink"));
	}
	
	/// Destructor
	~Combine(){

	}

	/// Function templates
	Item* fusion(string item1, string item2);
};

#endif
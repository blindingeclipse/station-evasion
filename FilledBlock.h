#ifndef FILLEDBLOCK
#define FILLEDBLOCK

#include <iostream>

#include "MapLocation.h"
#include "Puzzle.h"

using namespace std;

/**
 * FilledBlocks are impassable map locations
 * A.K.A "Wall"
 */
class FilledBlock : public MapLocation{
private:

public:
	/// Constructor
	FilledBlock(int r, int c)
	: MapLocation(r,c,"Wall")
	{
		setDescription("This is a Wall...");
		setPassability(false);
	}

	/// Destructor
	~FilledBlock(){

	}

	/// Function templates
	void addContainer(string desc){}
	Container* accessContainer(string desc){return NULL;}
	void displayContainers(ostream& os){}
	void setNPC(Npc* anNpc){}
	Npc* accessNPC(){return NULL;}
	void displayNPC(ostream& os){}
	void subSave(ofstream& outFile){}
	void subLoad(ifstream& inFile){}
};

#endif

#include <iostream>
#include <cstdlib>
#include <ctime>
#include <fstream>

#include "Alien.h"

using namespace std;

int Alien::getRow(){
	return row;
}

int Alien::getCol(){
	return col;
}

int Alien::move(){
	srand(time(NULL));

	return (rand() % 4) + 1;
}

///Saves the aliens's location, health and inventory
void Alien::save(ofstream& outFile){

	outFile << row << " " << col << " ";
}

///Loads the aliens's location, health, and inventory
void Alien::load(ifstream& inFile){

	inFile >> row >> col;
}
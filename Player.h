#ifndef PLAYER
#define PLAYER

#include <iostream>
#include <vector>
#include <string>
#include "Item.h"
#include "Exceptions.h"

using namespace std;

class Player{
private:
	int row, col, health, maxHealth;
	vector<Item*> inventory;
	bool win = false;
public:
	/// Constructors
	Player(){
		row = 1;
		col = 1;
		health = 1;
		maxHealth = 100;
	}

	Player(int r, int c, int h, int mH){
		row = r;
		col = c;
		health = h;
		maxHealth = mH;
		win = false;
	}

	/// Destructor
	~Player(){
		// for(unsigned int i=0; i < inventory.size(); i++){
		// 	delete inventory[i];
		// }
	}

	/// Function Templates
	int getRow();
	int getCol();
	void moveNorth();
	void moveEast();
	void moveSouth();
	void moveWest();
	void addItem(string name);
	void addItem(string name, int d);
	void addItem(string name, bool c, int d);
	void addItem(Item* i);
	Item* removeItem(string name);
	bool hasItem(string item);
	bool useItem(string item);
	void displayItems(ostream& os);
	void displayStats(ostream& os);
	void heal(int hp);
	void damage(int hp);
	bool isAlive();
	int inventorySize();
	void save(ofstream& outFile);
	void load(ifstream& inFile);
	bool hazWon();
	void didWin();
};

#endif

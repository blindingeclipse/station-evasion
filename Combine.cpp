#include <iostream>
#include <string>

#include "Combine.h"

using namespace std;

Item* Combine::fusion(string item1, string item2){

bool found;
string item = "";

	if ( combinations.find(item1) == combinations.end() ) {
	    // not found
		found = false;
	} else {
	    // found
		if(item2.compare(combinations.at(item1)) == 0){
			found = true;
			item += item1;
			item += item2;
		}else{
			found = false;
		}
	}

	if(!found){
		if ( combinations.find(item2) == combinations.end() ) {
		    // not found
			found = false;
		} else {
		    // found
			if(item1.compare(combinations.at(item2)) == 0){
				found = true;
				item += item2;
				item += item1;
			}else{
				found = false;
			}
		}
	}

	if(found){
		cout << endl << "FUSION!!!! You got " << item << endl << endl;
		Item* i = new Item(item);
		return i;
	}else{
		return NULL;
	}
}
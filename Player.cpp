#include <iostream>
#include <string>
#include <fstream>

#include "Player.h"

using namespace std;

int Player::getRow(){
	return row;
}

int Player::getCol(){
	return col;
}

void Player::moveNorth(){
	row++;
}

void Player::moveEast(){
	col++;
}

void Player::moveSouth(){
	row--;
}

void Player::moveWest(){
	col--;
}

///Adds an item to the player's inventory (Non-Consumable)
void Player::addItem(string name){
	Item* i = new Item(name);
	inventory.push_back(i);
}

///Adds an item to the player's inventory (Consumable)
void Player::addItem(string name, int d){
	Item* i = new Item(name, d);
	inventory.push_back(i);
}

///Adds an item to the player's inventory (Variably Consumable)
void Player::addItem(string name, bool c, int d){
	Item* i = new Item(name, c, d);
	inventory.push_back(i);
}

void Player::addItem(Item* i){
	inventory.push_back(i);
}

///Remove an item from the player's inventory and return it
Item* Player::removeItem(string name){
	for(unsigned int i=0; i<inventory.size(); i++){
		if(name.compare(inventory[i]->getName()) == 0){
			Item* temp = inventory[i];
			inventory.erase(inventory.begin()+i);
			return temp;
		}
	}
	return NULL;
}

///Checks if the player has an item by comparing item names
bool Player::hasItem(string item){
	try{
		for(unsigned int i=0; i< inventory.size(); i++){
			if(inventory[i]->getName().compare(item) == 0){
				return true;
			}
		}
		throw invalid_item_error("You don't have that in your inventory... Awkward\n\n");
	}catch(invalid_item_error& err){
		cout << err.what();
	}
	return false;
}

/**Attempts to use an item in the player's inventory, returns true if it exists.
 * Destroys the item if durability reaches 0
 */
bool Player::useItem(string item){
	// cout << item;
	bool retained;
	try{
		for(unsigned int i=0; i< inventory.size(); i++){
			if(inventory[i]->getName().compare(item) == 0){
				retained = inventory[i]->use();
				if(!retained){
					inventory.erase(inventory.begin()+i);
				}
				return true;
			}
		}
		throw invalid_item_error("You don't have that in your inventory... Awkward\n\n");
	}catch(invalid_item_error& err){
		cout << err.what();
	}
	return false;
}

/**
 * Shows the player a list of items in their inventory, occurs when the player "looks" at their inventory
 */
void Player::displayItems(ostream& os){
	if(inventory.size() == 0){
		os << "You don't have anything in your inventory\n\n";
	}else{
		os << "You have :\n";
		for(unsigned int i=0; i<inventory.size(); i++){
			os << " -" << inventory[i]->getDescription() << endl;
		}
		os << endl << endl;
	}
}

///Displays the player's health
void Player::displayStats(ostream& os){
	os << "Health: " << health << endl;
}

///Adds health to the player up to a maximum
void Player::heal(int hp){
	health += hp;
	if(health > maxHealth){
		health = maxHealth;
	}
}

///Removes health from a player
void Player::damage(int hp){
	health -= hp;
}

///Checks if the player's health is above 0
bool Player::isAlive(){
	if(health > 0)
		return true;
	else
		return false;
}

///Returns the number of items in the player's inventory
int Player::inventorySize(){
	return inventory.size();
}

///Saves the player's location, health and inventory
void Player::save(ofstream& outFile){

	outFile << row << " " << col << " " << health << " " << maxHealth << " ";

	outFile << inventory.size() << " ";
	for(unsigned int i=0; i<inventory.size(); i++){
		inventory[i]->save(outFile);
	}
}

///Loads the player's location, health, and inventory
void Player::load(ifstream& inFile){


	inFile >> row >> col >> health >> maxHealth;
	int s;
	inFile >> s;
	inventory.clear();
	inventory.reserve(s);

	for(int i=0; i<s; i++){
		inventory.push_back(new Item());
		inventory[i]->load(inFile);
	}
}

bool Player::hazWon(){
	return win;
}

void Player::didWin(){
	win = true;
}
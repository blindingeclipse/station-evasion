#ifndef TESTPLAYER_H
#define TESTPLAYER_H

#include <cppunit/extensions/HelperMacros.h>

#include "../Item.h"
#include "../Exceptions.h"
#include "../Player.h" 
#include <vector>
#include <string>
#include <iostream>

using namespace std; 

class TestPlayer : public CppUnit::TestFixture {
	CPPUNIT_TEST_SUITE( TestPlayer ); 
	CPPUNIT_TEST( testConstructor ); 
	CPPUNIT_TEST( testMoveNorth ); 
	CPPUNIT_TEST( testMoveSouth ); 
	CPPUNIT_TEST( testMoveEast ); 
	CPPUNIT_TEST( testMoveWest ); 
	CPPUNIT_TEST( testAddItem ); 
	CPPUNIT_TEST( testAddItem2 ); 	
	CPPUNIT_TEST( testAddItem3 ); 
	CPPUNIT_TEST( testRemoveItem ); 
	CPPUNIT_TEST( testWin); 
	CPPUNIT_TEST_SUITE_END(); 

	Player* p1; 
	int row, col, health, maxHealth; 
	vector<Item*> inventory; 
	bool win; 

public: 
	void setUp(); 
	void tearDown(); 
	void testConstructor(); 
	void testMoveNorth(); 
	void testMoveSouth(); 
	void testMoveEast(); 
	void testMoveWest(); 
	void testAddItem(); 
	void testAddItem2();
	void testAddItem3(); 
	void testRemoveItem(); 
	void testHealth(); 
	void testWin(); 
};

#endif

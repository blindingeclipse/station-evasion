#include "TestPlayer.h"

//Registers the fixture into the 'registry'
CPPUNIT_TEST_SUITE_REGISTRATION( TestPlayer );

void TestPlayer::setUp() {
	p1 = new Player(); //starts with default values 
}

void TestPlayer::tearDown() {
	delete p1; 	
	for(unsigned int i=0; i <inventory.size(); i++)
		inventory.erase(inventory.begin()+i); 
}

void TestPlayer::testConstructor() {

}

void TestPlayer::testMoveNorth() {
	int temp = p1->getRow(); 
	p1->moveNorth(); 
	CPPUNIT_ASSERT_EQUAL(p1->getRow(), temp+1); 
}

void TestPlayer::testMoveSouth() {
	int temp = p1->getRow(); 
	p1->moveSouth(); 
	CPPUNIT_ASSERT_EQUAL(p1->getRow(), temp-1); 
}

void TestPlayer::testMoveEast() {
	int temp = p1->getCol(); 
	p1->moveEast(); 
	CPPUNIT_ASSERT_EQUAL(p1->getCol(), temp+1); 
}

void TestPlayer::testMoveWest() {
	int temp = p1->getCol(); 
	p1->moveWest(); 
	CPPUNIT_ASSERT_EQUAL(p1->getCol(), temp-1); 
}

void TestPlayer::testAddItem() {
	//Test non consumable add item
	// Item* temp = new Item(); 		
	p1->addItem("testString");
	CPPUNIT_ASSERT(p1->hasItem("testString") == true); 
	CPPUNIT_ASSERT(p1->inventorySize() == 1); 	
}

void TestPlayer::testAddItem2() {
	//test consumable item 
	p1->addItem("testString2", 3);
	CPPUNIT_ASSERT(p1->hasItem("testString2") == true); 
	CPPUNIT_ASSERT(p1->inventorySize() == 2); 	
}

void TestPlayer::testAddItem3() {
	//test variably consumable item 
	p1->addItem("testString3", false, 3);
	CPPUNIT_ASSERT(p1->hasItem("testString3") == true); 
	CPPUNIT_ASSERT(p1->inventorySize() == 3); 	
}

void TestPlayer::testRemoveItem() {
	Item* temp = new Item(); 
	temp = p1->removeItem("testString"); 
	CPPUNIT_ASSERT(temp->getName() == "testString" );
	delete temp; 
}
void TestPlayer::testHealth() {
	p1->damage(1); 
	CPPUNIT_ASSERT(p1->isAlive() == false );

	p1->heal(51); 
	CPPUNIT_ASSERT(p1->isAlive() == true); 
}

void TestPlayer::testWin() {
	CPPUNIT_ASSERT(p1->hazWon() == false); 
	p1->didWin(); 
	CPPUNIT_ASSERT(p1->hazWon() == true); 	
}
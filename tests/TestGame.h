#ifndef TESTGAME_H
#define TESTGAME_H

#include <cppunit/extensions/HelperMacros.h>

//#include "Generic"

class TestGame : public CppUnit::TestFixture {
	CPPUNIT_TEST_SUITE( TestGame ); 
	CPPUNIT_TEST( testConstructor ); 
	CPPUNIT_TEST( testResolvePuzzle ); 
	CPPUNIT_TEST_SUITE_END(); 
	Map testMap;
	Player testPlayer; 
	Alien testAlien; 
	Combine testCombinator;
	Seperate testSeperator; 

public: 
	void setUp(); 
	void tearDown(); 
	void testConstructor(); 
	void testResolvePuzzle(); 
	

};

#endif

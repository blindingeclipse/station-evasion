#ifndef ITEM
#define ITEM

#include <iostream>
#include <string>

using namespace std;

class Item{
private:
	string name;
	bool consumable;
	int durability;

public:
	/// Constructors
	Item() {

	}
    ///Non-consumable items
	Item(string desc) {
		name.assign(desc);
		consumable = false;
	}
    ///Consumable items
	Item(string desc, int d) {
		name.assign(desc);
		consumable = true;
		durability = d;
	}
    ///Variably consumable items
	Item(string desc, bool c, int d) {
		name.assign(desc);
		consumable = c;
		durability = d;
	}

	/// Destructor
	~Item(){

	}

	/// Function templates
	string getDescription();
	string getName();
	void setItem(string description);
	void setItem(string description, int d);
	bool use();
	void save(ofstream& outFile);
	void load(ifstream& inFile);
};

#endif

#ifndef GAME
#define GAME

#include <iostream>
#include <string>
#include <vector>
#include <map>

#include "NPC.h"
#include "Combine.h"
#include "Separate.h"
#include "Exceptions.h"
#include "Item.h"
#include "Container.h"
#include "Map.h"
#include "MapLocation.h"
#include "Door.h"
#include "Player.h"
#include "Alien.h"

using namespace std;

class Game{
private:
    ///Create the game objects
	Map gameMap;
	Player player;
	Alien alien;
	Combine combinator;
	Separate separator;
	// int (*funcPointer)(vector < string > );
	// map<string,funcPointer> functionMap;

public:
	/// Constructor
	Game(){
		// Map, Player, and Alien are constructed above and as a result their
		// coordinates are hard coded in their constructors accepting ()
	}

	/// Destructor
	~Game(){

	}

	/// Function Templates
	void play();
	void setup();
	MapLocation* spaceOf(Player player);
	MapLocation* spaceOf(Alien alien);
	void displaySurroundings(ostream& os);
	bool resolvePuzzle(Puzzle* puzzle);
	void resolveTrap(Trap* trap);
	void resolveAction(string act, string subject);
	void goCommand(string direction);
	void lookCommand(string object);
	void useCommand(string object);
	void combineCommand(string object);
	void separateCommand(string object);
	void talkCommand(string object);
	void saveCommand();
	void save();
	void loadCommand();
	void load();
	void helpCommand();
	void tryMove(string direction);
	void itemExchangeMenu(Container* container);
	void takeItem(Container* container, string item);
	void putItem(Container* container, string item);
	bool itsNotOverYet();
	void resolveAlien();
};

#endif

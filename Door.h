#ifndef DOOR
#define DOOR

#include <iostream>

#include "MapLocation.h"
#include "Puzzle.h"
#include "NPC.h"

using namespace std;

/**
 * Doors are passable map locations
 */
class Door : public MapLocation {
private:

public:
	/// Constructor
	Door(int r, int c)
	: MapLocation(r,c,"Door")
	{
		setDescription("You see a real spooky door");
		setPassability(true);
	}

	/// Destructor
	~Door(){

	}

	/// Function templates
	void addContainer(string desc){}
	Container* accessContainer(string desc){return NULL;}
	void displayContainers(ostream& os){}
	void setNPC(Npc* anNpc){}
	Npc* accessNPC(){return NULL;}
	void displayNPC(ostream& os){}
	void subSave(ofstream& outFile){}
	void subLoad(ifstream& inFile){}
};

#endif

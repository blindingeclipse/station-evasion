CC=g++11
CFLAGS=-Wall -g -std=c++11 -fprofile-arcs -ftest-coverage

TEST_SRC=tests

OBJ_DIR=obj
OBJS= main.o Game.o Map.o MapLocation.o Room.o Door.o Puzzle.o Trap.o NPC.o Container.o Item.o Combine.o Separate.o Player.o Alien.o
TEST_OBJS= $(TEST_SRC)/RunTests.o $(TEST_SRC)/TestPlayer.o 
##$(TEST_SRC)/TestAlien.o $(TEST_SRC)/TestCombine.o $(TEST_SRC)/TestContainer.o  $(TEST_SRC)/TestDoor.o $(TEST_SRC)/TestItem.o $(TEST_SRC)/TestMap.o $(TEST_SRC)/TestMapLocation.o $(TEST_SRC)/TestNPC.o $(TEST_SRC)/TestPuzzle.o $(TEST_SRC)/TestRoom.o $(TEST_SRC)/TestSeperate.o $(TEST_SRC)/TestTrap.o 

INCLUDE= -I .

GCOV = gcov11
COVERAGE_RESULTS = result.coverage

PROGRAM=A_Bored_Jalapeno.exe
PROGRAM_TEST=testApp

.PHONY: all
all: $(PROGRAM) $(PROGRAM_TEST)

$(PROGRAM): $(OBJS) main.o
	$(CC) $(CFLAGS) -o $@ $^

# default rule for compiling .cc to .o
%.o: %.cpp
	$(CC) $(CFLAGS) -c $< -o $(patsubst %.cpp,%.o,$<) ${INCLUDE}


## generate the prerequistes and append to the desired file
.prereq : $(OBJS:.o=.cpp) $(wildcard *.h) Makefile
	rm -f .prereq
	$(CC) $(CCFLAGS) -MM $(OBJS:.o=.cpp) >> ./.prereq 

    ## include the generated prerequisite file
     include .prereq

.PHONY: clean
clean:
	rm -rf *~ *.o $(TEST_SRC)/*.o *.gcov $(TEST_SRC)/*.gcov *.gcda $(TEST_SRC)/*.gcda *.gcno $(TEST_SRC)/*.gcno $(COVERAGE_RESULTS)

.PHONY: clean-all
clean-all: clean
	rm -rf $(PROGRAM) $(PROGRAM_TEST) 

run: $(PROGRAM)
	$(PROGRAM)
	
memcheck: $(PROGRAM)
	valgrind --leak-check=yes $(PROGRAM)

$(PROGRAM_TEST): $(TEST_OBJS) $(OBJS)
	$(CC) $(CFLAGS) -o $@ $^ -lcppunit $(INCLUDE) 

test: $(PROGRAM_TEST)
	$(PROGRAM_TEST)
	
test-memcheck: $(PROGRAM_TEST)
	rm -f results
	valgrind --leak-check=yes $(PROGRAM_TEST) &> results
	more results

coverage: test	
	$(GCOV) *.cpp 
	cd $(TEST_SRC)
	$(GCOV) *.cpp
	cd ..
	grep -r "####" *.cpp.gcov > $(COVERAGE_RESULTS)
	more $(COVERAGE_RESULTS)

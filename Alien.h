#ifndef ALIEN
#define ALIEN

#include <iostream>

using namespace std;

class Alien{
private:

public:
	int row, col;

	/// Constructor
	Alien(){
		// cout << "Alien Constructed\n";
		row = 1;
		col = 5;
	}

	/// Destructor
	~Alien(){
		
	}

	/// Function Templates
	int getRow();
	int getCol();
	int move();
	void save(ofstream& outFile);
	void load(ifstream& inFile);
};

#endif